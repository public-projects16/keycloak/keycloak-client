import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { KeycloakService } from '../../services/keycloak.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public message: string;
  public name: string;
  public mail: string;
  public accessToken: string;

  constructor(private data: DataService, private keycloak: KeycloakService) { }

  ngOnInit() {
    this.data.getWelcomeMessage().subscribe((response: any) => {
      this.message = response.data.info;
      this.name = response.data.name;
      this.mail = response.data.mail;
    });
    this.accessToken = this.keycloak.getToken();
  }

  closeSession() {
    this.keycloak.logOut();
  }

}
