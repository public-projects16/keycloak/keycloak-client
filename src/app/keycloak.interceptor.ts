import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { KeycloakService } from './services/keycloak.service';

@Injectable()
export class KeycloakInterceptor implements HttpInterceptor {

    constructor(private keycloak: KeycloakService) { }
    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authToken = this.keycloak.getToken() || '';
        req = req.clone({
            headers: new HttpHeaders({
                Authorization : 'Bearer ' + authToken
            })
        });
        return next.handle(req);
    }
}
