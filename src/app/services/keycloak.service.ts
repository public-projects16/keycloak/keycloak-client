import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import * as Keycloak from 'keycloak-js';

@Injectable()

export class KeycloakService {

  private keycloakAuth: any;

  constructor() { }

  init(): Promise<any> {
    return new Promise((resolve, reject) => {
      const config = {
        url: environment.K_URL,
        realm: environment.K_REALM,
        clientId: environment.K_CLIENT
      };
      this.keycloakAuth =  Keycloak(config);
      this.keycloakAuth.init({ onLoad: environment.K_ONLOAD })
        .success(() => {
          resolve();
        })
        .error(() => {
          reject();
        });
    });
  }

  getToken(): string {
    return this.keycloakAuth.token;
  }

  logOut() {
    return this.keycloakAuth.logout();
  }
}

